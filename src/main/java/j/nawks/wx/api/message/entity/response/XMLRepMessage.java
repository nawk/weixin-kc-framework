package j.nawks.wx.api.message.entity.response;

import j.nawks.wx.api.message.support.MessageType;

import java.util.Date;

/**
 * 普通相应消息抽象类
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public abstract class XMLRepMessage implements RepMessage {

	/**
	 * 接收方帐号（收到的OpenID）
	 */
	private String toUserName;
	/**
	 * 开发者微信号
	 */
	private String fromUserName;
	/**
	 * 消息创建时间 （整型）
	 */
	private Date createTime;

	/**
	 * 消息类型
	 */
	private MessageType msgType;

	public String toPlainText() {
		return null;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public MessageType getMsgType() {
		return msgType;
	}

	public void setMsgType(MessageType msgType) {
		this.msgType = msgType;
	}
}
