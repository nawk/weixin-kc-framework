package j.nawks.wx.api.message.entity.request;

/**
 * 语音消息
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class VoiceMessage extends AbstractCommonMessage {

	/**
	 * 语音消息媒体id，可以调用多媒体文件下载接口拉取数据
	 */
	private String mediaId;

	/**
	 * 语音格式，如amr，speex等
	 */
	private String format;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
