package j.nawks.wx.api.message.entity.request;

/**
 * 普通消息类型
 * 
 * @author Nawks.J@Gmail.com
 * 
 */
public abstract class AbstractCommonMessage extends ReqMessage {
	/**
	 * 消息id，64位整型
	 */
	private Long msgId;

	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}
}
