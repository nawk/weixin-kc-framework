package j.nawks.wx.api.message.convert;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalTypeConvert implements Convert {

	public Object convert(String plainText) {
		BigDecimal data = null;

		try {
			data = new BigDecimal(plainText);
			data.setScale(8, RoundingMode.HALF_EVEN);
		} catch (Exception e) {
		}
		return data;
	}

}
