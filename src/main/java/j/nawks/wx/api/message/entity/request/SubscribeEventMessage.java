package j.nawks.wx.api.message.entity.request;

/**
 * 订阅事件
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 * @see http://mp.weixin.qq.com/wiki/index.php?title=%E6%8E%A5%E6%94%B6%E4%BA%8B%E4%BB%B6%E6%8E%A8%E9%80%81
 */
public class SubscribeEventMessage extends AbstractEventMessage {

	/**
	 * 事件KEY值，qrscene_为前缀，后面为二维码的参数值
	 */
	private String eventKey;
	/**
	 * 二维码的ticket，可用来换取二维码图片
	 */
	private String ticket;
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
