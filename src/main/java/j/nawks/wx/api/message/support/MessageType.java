package j.nawks.wx.api.message.support;

/**
 * 微信消息类型
 * 
 * @author Nawks.J@Gmail.com
 * 
 */
public enum MessageType {
	EVENT("event"), // 事件消息
	TEXT("text"), // 文本消息
	IMAGE("image"), // 图片消息
	VOICE("voice"), // 语音消息
	VIDEO("video"), // 视频消息
	LOCATION("location"), // 地理位置消息
	LINK("link");// 链接消息

	private String value;

	private MessageType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static MessageType getType(String value) {
		if (EVENT.value.equals(value)) {
			return EVENT;
		} else if (TEXT.value.equals(value)) {
			return TEXT;
		} else if (VOICE.value.equals(value)) {
			return VOICE;
		} else if (VIDEO.value.equals(value)) {
			return VIDEO;
		} else if (LOCATION.value.equals(value)) {
			return LOCATION;
		} else if (LINK.value.equals(value)) {
			return LINK;
		}
		return null;
	}
}
