package j.nawks.wx.api.message.support;

/**
 * 事件类型
 * 
 * @author Nawks.J@Gmail.com
 * 
 */
public enum EventType {
	SUBSCRIBE("subscribe", "关注"), 
	UNSUBSCRIBE("unsubscribe", "取消关注"), 
	SCAN("scan", ""), 
	LOCATION("location", "获取地理位置"), 
	CLICK("click","自定义菜单事件"), 
	VIEW("view", "点击菜单跳转链接时的事件推送");
	
	private String value;
	private String desc;

	private EventType(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}
	
	public static EventType getType(String value){
		if(SUBSCRIBE.value.equals(value)){
			return SUBSCRIBE;
		} else if(UNSUBSCRIBE.value.equals(value)){
			return UNSUBSCRIBE;
		} else if(SCAN.value.equals(value)){
			return SCAN;
		} else if(LOCATION.value.equals(value)){
			return LOCATION;
		} else if(CLICK.value.equals(value)){
			return CLICK;
		} else if(VIEW.value.equals(value)){
			return VIEW;
		}
		return null;
	}
}
