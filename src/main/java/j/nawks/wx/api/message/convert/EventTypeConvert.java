package j.nawks.wx.api.message.convert;

import j.nawks.wx.api.message.support.EventType;

public class EventTypeConvert implements Convert {

	public Object convert(String plainText) {
		return EventType.getType(plainText);
	}

}
