package j.nawks.wx.api.message.entity.response.xml;

import j.nawks.wx.api.message.entity.response.XMLRepMessage;

/**
 * 回复图片消息
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class ImageMessage extends XMLRepMessage {

	/**
	 * 通过上传多媒体文件，得到的id
	 */
	private Long mediaId;

	public Long getMediaId() {
		return mediaId;
	}

	public void setMediaId(Long mediaId) {
		this.mediaId = mediaId;
	}

}
