package j.nawks.wx.api.message.entity.request;

import j.nawks.wx.api.message.support.EventType;

/**
 * 事件实体基础类
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class AbstractEventMessage extends ReqMessage {

	/**
	 * 事件类型
	 */
	private EventType event;

	public EventType getEvent() {
		return event;
	}

	public void setEvent(EventType event) {
		this.event = event;
	}
}
