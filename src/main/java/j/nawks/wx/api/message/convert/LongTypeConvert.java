package j.nawks.wx.api.message.convert;

public class LongTypeConvert implements Convert {

	public Object convert(String plainText) {
		Long data = null;
		try {
			data = Long.parseLong(plainText);
		} catch (NumberFormatException e) {
		}

		return data;
	}

}
