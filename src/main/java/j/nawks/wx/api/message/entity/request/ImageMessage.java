package j.nawks.wx.api.message.entity.request;

/**
 * 图片消息
 * 
 * @author Nawks.J@Gmail.com
 * 
 */
/**
 * @author Nawks.J@Gmail.com
 * 
 */
public class ImageMessage extends AbstractCommonMessage {
	/**
	 * 图片链接
	 */
	private String picUrl;
	/**
	 * 图片消息媒体id，可以调用多媒体文件下载接口拉取数据
	 */
	private String mediaId;

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
}
