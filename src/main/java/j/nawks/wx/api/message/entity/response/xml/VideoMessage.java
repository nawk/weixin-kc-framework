package j.nawks.wx.api.message.entity.response.xml;

import j.nawks.wx.api.message.entity.response.XMLRepMessage;

/**
 * 回复视频消息
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class VideoMessage extends XMLRepMessage {

	/**
	 * 通过上传多媒体文件，得到的id
	 */
	private String mediaId;
	/**
	 * 视频消息的标题
	 */
	private String title;
	/**
	 * 视频消息的描述
	 */
	private String description;
}
