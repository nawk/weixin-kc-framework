package j.nawks.wx.api.message.support;

public class WeixinConstant {
	// 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数;
	public final static String PARAM_SIGNATURE = "signature";
	// 时间戳
	public final static String PARAM_TIMESTAMP = "timestamp";
	// 随机数
	public final static String PARAM_NONCE = "nonce";
	// 随机字符串
	public final static String PARAM_ECHOSTR = "echostr";
}
