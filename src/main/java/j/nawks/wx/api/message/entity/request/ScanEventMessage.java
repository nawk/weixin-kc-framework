package j.nawks.wx.api.message.entity.request;

/**
 * 用户已关注时的事件推送
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class ScanEventMessage extends AbstractEventMessage {

	/**
	 * 事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id
	 */
	private Long eventKey;
	/**
	 * 二维码的ticket，可用来换取二维码图片
	 */
	private String ticket;
	public Long getEventKey() {
		return eventKey;
	}
	public void setEventKey(Long eventKey) {
		this.eventKey = eventKey;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
