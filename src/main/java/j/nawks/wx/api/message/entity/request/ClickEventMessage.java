package j.nawks.wx.api.message.entity.request;

/**
 * 自定义菜单事件
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014 <br/>
 *       用户点击自定义菜单后，微信会把点击事件推送给开发者，请注意，点击菜单弹出子菜单，不会产生上报。
 */
public class ClickEventMessage extends AbstractEventMessage {

	/**
	 * 事件KEY值，与自定义菜单接口中KEY值对应
	 */
	private String eventKey;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
}
