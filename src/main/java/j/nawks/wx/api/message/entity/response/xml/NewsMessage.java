package j.nawks.wx.api.message.entity.response.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import j.nawks.wx.api.exception.ArticleExceedException;
import j.nawks.wx.api.message.entity.response.XMLRepMessage;

/**
 * 回复图文消息
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class NewsMessage extends XMLRepMessage {
	private List<ArticleItem> items;

	public NewsMessage() {
		this.items = new ArrayList<ArticleItem>(10);
	}

	/**
	 * 图文消息个数，限制为10条以内
	 */
	public int getArticleCount() {
		return this.items.size();
	}

	public void setAricles(List<ArticleItem> articles) {
		if (articles != null) {
			if (articles.size() > 10) {
				int idx = 0;
				while (this.items.size() < 10) {
					this.items.add(articles.get(idx++));
				}
			} else {
				this.items = articles;
			}
		}
	}

	public void addArticle(ArticleItem article) {
		if (this.items.size() < 10) {
			this.items.add(article);
		} else {
			throw new ArticleExceedException("The article is too many!!");
		}
	}
}
