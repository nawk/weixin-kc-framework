package j.nawks.wx.api.message.entity.response.xml;

import j.nawks.wx.api.message.entity.response.XMLRepMessage;

/**
 * 回复文本消息
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class TextMessage extends XMLRepMessage {

	/**
	 * 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
	 */
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
