package j.nawks.wx.api.message.convert;

import j.nawks.wx.api.message.support.MessageType;

/**
 * 消息类型转换器
 * 
 * @author Nawks.J@Gmail.com
 * 
 */
public class MessageTypeConvert implements Convert {

	public Object convert(String plainText) {
		return MessageType.getType(plainText);
	}

}
