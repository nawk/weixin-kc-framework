package j.nawks.wx.api.message.entity.request;

/**
 * 普通文本消息
 * 
 * @author Nawks.J@Gmail.com
 * 
 */
public class TextMessage extends AbstractCommonMessage {

	/**
	 * 文本消息内容
	 */
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
