package j.nawks.wx.api.message.entity.response.xml;

import j.nawks.wx.api.message.entity.response.XMLRepMessage;

/**
 * 回复语音消息
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class VoiceMessage extends XMLRepMessage {

	/**
	 * 通过上传多媒体文件，得到的id
	 */
	private String mediaId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
}
