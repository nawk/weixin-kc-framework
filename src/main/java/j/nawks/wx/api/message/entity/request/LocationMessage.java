package j.nawks.wx.api.message.entity.request;

import java.math.BigDecimal;

/**
 * 地理位置消息
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class LocationMessage extends AbstractCommonMessage {
	/**
	 * 地理位置维度
	 */
	private BigDecimal location_X;
	/**
	 * 地理位置经度
	 */
	private BigDecimal location_Y;
	/**
	 * 地图缩放大小
	 */
	private BigDecimal scale;
	/**
	 * 地理位置信息
	 */
	private String label;

	public BigDecimal getLocation_X() {
		return location_X;
	}

	public void setLocation_X(BigDecimal location_X) {
		this.location_X = location_X;
	}

	public BigDecimal getLocation_Y() {
		return location_Y;
	}

	public void setLocation_Y(BigDecimal location_Y) {
		this.location_Y = location_Y;
	}

	public BigDecimal getScale() {
		return scale;
	}

	public void setScale(BigDecimal scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
