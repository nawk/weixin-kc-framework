package j.nawks.wx.api.message.convert;

/**
 * @author Nawks.J@Gmail.com
 * 
 */
public interface Convert {
	public Object convert(String plainText);
}
