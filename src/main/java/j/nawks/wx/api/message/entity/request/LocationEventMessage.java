package j.nawks.wx.api.message.entity.request;

import java.math.BigDecimal;

/**
 * 上报地理位置事件
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014 <br/>
 *       用户同意上报地理位置后，每次进入公众号会话时，都会在进入时上报地理位置，或在进入会话后每5秒上报一次地理位置，
 *       公众号可以在公众平台网站中修改以上设置。上报地理位置时，微信会将上报地理位置事件推送到开发者填写的URL
 */
public class LocationEventMessage extends AbstractEventMessage {

	/**
	 * 地理位置纬度
	 */
	private BigDecimal latitude;
	/**
	 * 地理位置经度
	 */
	private BigDecimal longitude;
	/**
	 * 地理位置精度
	 */
	private BigDecimal precision;
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public BigDecimal getPrecision() {
		return precision;
	}
	public void setPrecision(BigDecimal precision) {
		this.precision = precision;
	}
}
