package j.nawks.wx.api.message.entity.request;

/**
 * 点击菜单跳转链接时的事件推送
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class ViewEventMessage extends AbstractEventMessage {

	/**
	 * 事件KEY值，设置的跳转URL
	 */
	private String eventKey;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
}
