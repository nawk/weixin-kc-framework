package j.nawks.wx.api.message.convert;

import java.util.Date;

/**
 * 将扁平化的数字字符串转成日期格式
 * @author Nawks.J@Gmail.com
 *
 */
public class TimeTypeConvert implements Convert {
	public Object convert(String plainText) {
		return new Date(Long.parseLong(plainText));
	}

}
