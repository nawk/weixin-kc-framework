package j.nawks.wx.api.message.support;

public class MopaasConstant {

	public final static String MYSQL_HOST = "MOPAAS_MYSQL17193_HOST";
	public final static String MYSQL_PORT = "MOPAAS_MYSQL17193_PORT";
	public final static String MYSQL_DB = "MOPAAS_MYSQL17193_NAME";
	public final static String MYSQL_USERNAME = "MOPAAS_MYSQL17193_USERNAME";
	public final static String MYSQL_PASSWORD = "MOPAAS_MYSQL17193_PASSWORD";

}
