package j.nawks.wx.api.exception;

/**
 * 文章超过异常
 * 
 * @author Nawks.J@Gmail.com
 * @date Nov 23, 2014
 */
public class ArticleExceedException extends RuntimeException {

	public ArticleExceedException() {
	}

	public ArticleExceedException(String message) {
		super(message);
	}

	public ArticleExceedException(Throwable cause) {
		super(cause);
	}

	public ArticleExceedException(String message, Throwable cause) {
		super(message, cause);
	}

}
