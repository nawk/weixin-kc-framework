package j.nawks.wx.api.utils;

import java.util.Date;

import j.nawks.wx.api.message.entity.response.xml.TextMessage;
import j.nawks.wx.api.message.support.MessageType;

public class ReponseMessageUtils {

	public static TextMessage defaultUNMessage(String fromUser, String toUser) {
		TextMessage message = newTextMessage(fromUser, toUser);
		message.setContent("此功能正在开发中!!!");
		return message;
	}

	public static TextMessage defaultExceptionMessage(String fromUser,
			String toUser) {
		TextMessage message = newTextMessage(fromUser, toUser);
		message.setContent("系统异常!");
		return message;
	}

	public static TextMessage newTextMessage(String fromUser, String toUser) {
		TextMessage message = new TextMessage();
		message.setCreateTime(new Date());
		message.setFromUserName(fromUser);
		message.setToUserName(toUser);
		message.setMsgType(MessageType.TEXT);
		return message;
	}

}
