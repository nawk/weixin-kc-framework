package j.nawks.wx.api.utils;

public class MessageFormatUtils {

	// 发送方
	public final static String FROMUSERNAME = "FromUserName";
	public final static String TOUSERNAME = "ToUserName";
	public final static String MSGTYPE = "MsgType";
	public final static String CREATETIME = "CreateTime";

	public final static String EVENT_TYPE = "Event";
	public final static String EVENT_MESSAGE = "event";
}
