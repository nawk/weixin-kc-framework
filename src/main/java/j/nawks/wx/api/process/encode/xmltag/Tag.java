package j.nawks.wx.api.process.encode.xmltag;

public class Tag {
	private String tagName;

	public Tag(String tagName) {
		this.tagName = tagName;
	}

	public String getTagName() {
		return tagName;
	}
}
