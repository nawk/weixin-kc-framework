package j.nawks.wx.api.process.interceptor;

import j.nawks.wx.api.message.entity.request.ReqMessage;
import j.nawks.wx.api.process.ProcessContext;
import j.nawks.wx.api.utils.ReponseMessageUtils;

public class LastDefaultInterceptor extends AbstractInterceptor<ReqMessage> {

	public void doInterrcept(ReqMessage message, ProcessContext cxt) {
		if (cxt.getRepMessage() == null) {
			cxt.setRepMessage(ReponseMessageUtils.defaultUNMessage(
					message.getToUserName(), message.getFromUserName()));
		}
	}

}
