package j.nawks.wx.api.process;

import j.nawks.wx.api.message.entity.request.ReqMessage;

public interface Interceptor<T extends ReqMessage> {
	public void doInterrcept(T message, ProcessContext cxt);

}
