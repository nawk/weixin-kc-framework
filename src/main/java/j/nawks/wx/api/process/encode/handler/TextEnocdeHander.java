package j.nawks.wx.api.process.encode.handler;

import j.nawks.wx.api.message.entity.response.xml.TextMessage;
import j.nawks.wx.api.process.encode.EncodeContext;

public class TextEnocdeHander extends AbstractEncodeHandler<TextMessage> {
	public final static String XML_TAG_CONTENT = "Content";

	public void process(TextMessage repMessage, EncodeContext cxt) {
		cxt.pushTag(XML_TAG_CONTENT);
		cxt.pushPlainValue(repMessage.getContent());
		cxt.pushTag(XML_TAG_CONTENT);
		nextProcess(repMessage, cxt);
	}

}
