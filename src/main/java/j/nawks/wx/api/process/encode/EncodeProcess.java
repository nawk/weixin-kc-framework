package j.nawks.wx.api.process.encode;

import j.nawks.wx.api.message.entity.request.ReqMessage;
import j.nawks.wx.api.message.entity.response.RepMessage;
import j.nawks.wx.api.process.encode.handler.FirstEmptyHander;
import j.nawks.wx.api.process.encode.handler.TextEnocdeHander;
import j.nawks.wx.api.process.encode.handler.XMLEncodeHandler;

public class EncodeProcess {

	private static EncodeProcess process = new EncodeProcess();
	private EncodeHandler<RepMessage> handler;

	private EncodeProcess() {
		this._init();
	}

	private void _init() {
		this.handler = new FirstEmptyHander();

		EncodeHandler<? extends RepMessage> handler2, handler1;

		handler1 = this.handler;
		handler2 = new XMLEncodeHandler();
		handler1.setNextHandler(handler2);

		handler1 = handler2;
		handler2 = new TextEnocdeHander();
		handler1.setNextHandler(handler2);
	}

	public static EncodeProcess getInstance() {
		return process;
	}

	public String process(RepMessage message) {
		EncodeContext cxt = new EncodeContext();
		this.handler.process(message, cxt);
		return cxt.getPlainText();
	}
}
