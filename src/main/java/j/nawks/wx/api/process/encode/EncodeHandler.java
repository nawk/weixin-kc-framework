package j.nawks.wx.api.process.encode;

import j.nawks.wx.api.message.entity.response.RepMessage;

public interface EncodeHandler<T extends RepMessage> {

	public void setNextHandler(EncodeHandler<? extends RepMessage> handler2);

	public EncodeHandler<? extends RepMessage> getNextHandler();

	public void process(T repMessage, EncodeContext cxt);

	public void nextProcess(T repMessage, EncodeContext cxt);
}
