package j.nawks.wx.api.process.encode.xmltag;

import java.util.List;

public class CompositeTag extends Tag {

	private List<Tag> children;

	public CompositeTag(String tagName) {
		super(tagName);
	}

	public List<Tag> getChildren() {
		return children;
	}

	public void setChildren(List<Tag> children) {
		this.children = children;
	}
}
