package j.nawks.wx.api.process.encode.handler;

import j.nawks.wx.api.message.entity.response.RepMessage;
import j.nawks.wx.api.process.encode.EncodeContext;

public class FirstEmptyHander extends AbstractEncodeHandler<RepMessage> {

	public void process(RepMessage repMessage, EncodeContext cxt) {
		nextProcess(repMessage, cxt);
	}

}
