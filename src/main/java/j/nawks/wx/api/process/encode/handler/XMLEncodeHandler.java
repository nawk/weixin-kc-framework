package j.nawks.wx.api.process.encode.handler;

import j.nawks.wx.api.message.entity.response.XMLRepMessage;
import j.nawks.wx.api.process.encode.EncodeContext;

public class XMLEncodeHandler extends AbstractEncodeHandler<XMLRepMessage> {

	public final static String XML_TAG_ROOT = "xml";
	public final static String XML_TAG_TOUSERNAME = "ToUserName";
	public final static String XML_TAG_FROMUSERNAME = "FromUserName";
	public final static String XML_TAG_CREATETIME = "CreateTime";
	public final static String XML_TAG_MSGTYPE = "MsgType";

	public void process(XMLRepMessage repMessage, EncodeContext cxt) {
		/**
		 * 推入根节点
		 */
		cxt.pushTag(XML_TAG_ROOT);
		/**
		 * 推入接受者用户
		 */
		cxt.pushTag(XML_TAG_TOUSERNAME);
		cxt.pushPlainValue(repMessage.getToUserName());
		cxt.pushTag(XML_TAG_TOUSERNAME);

		/**
		 * 推入发送者用户信息
		 */
		cxt.pushTag(XML_TAG_FROMUSERNAME);
		cxt.pushPlainValue(repMessage.getFromUserName());
		cxt.pushTag(XML_TAG_FROMUSERNAME);

		/**
		 * 推入发送时间
		 */
		cxt.pushTag(XML_TAG_CREATETIME);
		cxt.pushPlainValue(String.valueOf(repMessage.getCreateTime().getTime()));
		cxt.pushTag(XML_TAG_CREATETIME);

		/**
		 * 推入消息类型
		 */
		nextProcess(repMessage, cxt);
		cxt.pushTag(XML_TAG_MSGTYPE);
		cxt.pushPlainValue(repMessage.getMsgType().getValue());
		cxt.pushTag(XML_TAG_MSGTYPE);

		cxt.pushTag(XML_TAG_ROOT);
	}

}
