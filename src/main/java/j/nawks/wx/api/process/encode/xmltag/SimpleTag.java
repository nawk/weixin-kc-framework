package j.nawks.wx.api.process.encode.xmltag;

public class SimpleTag extends Tag {

	private String value;

	public SimpleTag(String tagName) {
		super(tagName);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
