package j.nawks.wx.api.process.encode;

import java.util.Stack;

import org.apache.commons.lang3.StringUtils;

public class EncodeContext {
	private Stack<String> tagStack;
	private StringBuilder plainText;

	public EncodeContext() {
		this.tagStack = new Stack<String>();
		this.plainText = new StringBuilder();
	}

	public String getPlainText() {
		return plainText.toString();
	}

	public void pushTag(String tagName) {
		if (StringUtils.isNotBlank(tagName)) {
			if (!tagStack.isEmpty() && tagStack.peek().equals(tagName)) {
				this.plainText.append('<').append('/').append(tagStack.pop())
						.append('>');
			} else {
				tagStack.push(tagName);
				this.plainText.append('<').append(tagName).append('>');
			}
		}
	}

	public void pushPlainValue(String plainText) {
		this.plainText.append(plainText);
	}
}
