package j.nawks.wx.api.process;

import j.nawks.wx.api.message.entity.response.RepMessage;

import java.util.HashMap;
import java.util.Map;

public class ProcessContext {

	private Map<String, Object> valueStack;
	private RepMessage repMessage;

	public ProcessContext() {
		this.valueStack = new HashMap<String, Object>();
	}

	public Map<String, Object> getValueStack() {
		return valueStack;
	}

	public RepMessage getRepMessage() {
		return repMessage;
	}

	public void setRepMessage(RepMessage repMessage) {
		this.repMessage = repMessage;
	}

}
