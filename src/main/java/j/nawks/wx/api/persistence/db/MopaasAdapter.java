package j.nawks.wx.api.persistence.db;

import j.nawks.wx.api.message.support.MopaasConstant;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MopaasAdapter extends AbstractMysqlAdapter {

	private String host;
	private int port;
	private String dbName;
	private String username;
	private String password;
	private String url;

	@Override
	public Connection getConnection() throws ClassNotFoundException,
			SQLException {
		return DriverManager.getConnection(url, this.username, this.password);
	}

	@Override
	public void _init() throws ClassNotFoundException {
		this.host = System.getenv(MopaasConstant.MYSQL_HOST);
		this.port = Integer.parseInt(System.getenv(MopaasConstant.MYSQL_PORT));
		this.dbName = System.getenv(MopaasConstant.MYSQL_DB);
		this.username = System.getenv(MopaasConstant.MYSQL_USERNAME);
		this.password = System.getenv(MopaasConstant.MYSQL_PASSWORD);

		Class.forName("org.gjt.mm.mysql.Driver");
		this.url = String.format("jdbc:mysql://%s:%d/%s", this.host, this.port,
				this.dbName);

		System.out.println("url>>" + this.url);
		System.out.println("username>>" + this.username);
		System.out.println("password>>" + this.password);
	}

}
