package j.nawks.wx.api.persistence.mock;

import j.nawks.wx.api.message.entity.request.ReqMessage;
import j.nawks.wx.api.persistence.DataPeristence;

public class DefaultMockAdapter implements DataPeristence {

	public void log(String plainText, ReqMessage message) {
		System.out.println(String.format("%s-->%s", plainText,message!= null?message.toString():""));
	}

}
