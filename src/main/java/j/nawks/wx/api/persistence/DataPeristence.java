package j.nawks.wx.api.persistence;

import j.nawks.wx.api.message.entity.request.ReqMessage;

/**
 * 数据持久化接口
 * 
 * @author Nawks.J@Gmail.com
 * 
 */
public interface DataPeristence {
	public void log(String plainText, ReqMessage message);
}
