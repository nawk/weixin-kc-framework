package j.nawks.wx.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import j.nawks.wx.api.message.entity.request.TextMessage;
import j.nawks.wx.api.utils.ClassUtils;

import org.junit.Test;

public class ClassUtilsTest {

	@Test
	public void testGetAccessSetterMethods() {
		List<Method> methods = ClassUtils
				.getAccessSetterMethods(TextMessage.class);
		for (Method m : methods) {
			System.out.println(m.getName());
		}
		System.out.println(System.currentTimeMillis());
	}

	@Test
	public void testGetFields() {
		List<Field> fields = ClassUtils.getFields(TextMessage.class, true);
		for (Field field : fields) {
			System.out.println(field.getGenericType() + "-->" + field.getName());
		}
	}
}
