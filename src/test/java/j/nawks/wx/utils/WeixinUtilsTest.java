package j.nawks.wx.utils;

import static org.junit.Assert.*;
import j.nawks.wx.api.utils.WeixinUtils;

import org.junit.Test;

public class WeixinUtilsTest {

	@Test
	public void testCheckReqLegel() {
		assertEquals(WeixinUtils.checkReqLegel("ikongchan1234", "1416724522",
				"1974788626", "087b5c656aff851c890a7b1b684c4fd117079182"), true);
	}

}
