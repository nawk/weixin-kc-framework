package j.nawks.wx.api.process.encode;

import java.util.Date;

import j.nawks.wx.api.message.entity.response.xml.TextMessage;
import j.nawks.wx.api.message.support.MessageType;

import org.junit.Test;

public class EncodeProcessTest {

	@Test
	public void testProcess() {
		TextMessage message = new TextMessage();
		message.setToUserName("toUser");
		message.setFromUserName("fromUser");
		message.setCreateTime(new Date());
		message.setMsgType(MessageType.TEXT);
		message.setContent("这是文本信息");
		
		System.out.println(EncodeProcess.getInstance().process(message));
	}

}
